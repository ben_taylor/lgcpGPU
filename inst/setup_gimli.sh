#!/bin/bash

export CUDA_HOME=/usr/local/cuda-8.0
export PATH=/usr/local/cuda-8.0/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda-8.0/targets/x86_64-linux/lib:/usr/local/cuda-8.0/lib64:/usr/local/cuda-8.0/samples/common/inc/
