#define NK 18

struct timer {
    int ntimers;
    int lcr; // index of current timer (left=0, centre=1, right=2)
    int freq; // update frequency every freq + 1 iterations
    int count; // iteration counter
    int power[NK]; // this must match that set in initialiseTimer
    int realpower[NK]; // this must match that set in initialiseTimer
    int n_blocks[NK];
    int n_threads[NK];
    int minpow;
    int maxpow;
    int powerid;
    double timeleft;
    double timecentre;
    double timeright;
    clock_t start;
    clock_t end;
    int trigger; // 1 = abandon timing (e.g. large irregular operations) , 0 = carry on
};

int getNewBS(int oldpow, double timeleft, double timecentre, double timeright);
void updateTimer(timer *tim, int dim);
void initialiseTimer(timer *tim, int dim);
void abandonTiming(timer *tim);
void restartTimer(timer *tim);
void printTimer(timer *tim);
int log2int(int n);

int log2int(int n){
    // log(n)/log(2) is log2.
    return (int) log( (float) n ) / log( 2 );
}

int getNewBS(int oldpow, double timeleft, double timecentre, double timeright){
    if(timeleft > timecentre){
        if (timeright > timecentre){
            return oldpow;
        }
        else{
            return oldpow + 1;
        }
    }
    else{
        if(timeleft > timeright){
            return oldpow + 1;
        }
        else{
            if (oldpow > 0){
                return oldpow - 1;
            }
            else{
                return oldpow;
            }
        }
    }
}

void updateTimer(timer *tim, int dim){
    // int i = tim->powerid;

    // printf("%d ",tim->powerid);
    // for(i =0;i<NK;i++){
    //    printf("%d ",tim->power[i]);
    // }
    // printf("\n");

    // printf("%d ",tim->powerid);
    // for(i =0;i<NK;i++){
    //    printf("%d ",tim->n_blocks[i]);
    // }
    // printf("\n");
    // printf("%d ",tim->powerid);
    // for(i =0;i<NK;i++){
    //    printf("%d ",tim->n_threads[i]);
    // }
    // printf("\n");

    if(tim->lcr == 0){
        tim->power[tim->powerid] = tim->realpower[tim->powerid] - 1;
    }
    else if(tim->lcr == 1){
        tim->power[tim->powerid] = tim->realpower[tim->powerid];
    }
    else{
        tim->power[tim->powerid] = tim->realpower[tim->powerid] + 1;
    }

    if(tim->trigger == 1){ // abandon current timing effort
        restartTimer(tim);
    }
    else{
        if(tim->count == tim->freq){ // update (everything) power and reset counters
            if(tim->lcr == 2){
                tim->end = clock();
                tim->timeright = (double) tim->end - tim->start;
                int newpow = getNewBS(tim->realpower[tim->powerid],tim->timeleft,tim->timecentre,tim->timeright);
                //printf("id = %d, pow = % d, left=%.9g, centre=%.9g, right=%.9g\n",tim->powerid,tim->realpower[tim->powerid],tim->timeleft,tim->timecentre,tim->timeright);
                //printf("%d %d %.9g %.9g %.9g\n",tim->powerid,tim->realpower[tim->powerid],tim->timeleft,tim->timecentre,tim->timeright);
                if(newpow < tim->maxpow -1 && newpow > tim->minpow + 1){
                    tim->realpower[tim->powerid] = newpow;
                    tim->n_blocks[tim->powerid] = (int) pow(2.0f,(float) newpow);
                    tim->n_threads[tim->powerid] = dim / tim->n_blocks[tim->powerid];
                }
                tim->lcr = 0;
                tim->count = 0;
                if(tim->powerid == tim->ntimers - 1){
                    tim->powerid = 0;
                }
                else{
                    tim->powerid = tim->powerid + 1;
                }
                tim->start = clock(); // reset clock
            }
            else{ // record time and move onto next timer
                tim->end = clock();
                if(tim->lcr == 0){
                    tim->timeleft = (double) tim->end - tim->start;
                }
                else{ // tim->lcr == 1 by implication
                    tim->timecentre = (double) tim->end - tim->start;
                }
                tim->lcr = tim->lcr + 1;
                tim->count = 0;
                tim->start = clock();
            }
        }
        else{
            tim->count = tim->count + 1;
        }
    }
}

void initialiseTimer(timer *tim,int dim){ // intialise timer c.f. restartTiming below
    int nt = NK;// this must match the number of elements in the power above struct definition

    tim->minpow = log2int(dim/1024);
    tim->maxpow = log2int(dim);

    tim->ntimers = nt;
    tim->lcr = 0; // index of current timer (left=0, centre=1, right=2)
    tim->freq = 9; // update frequency every freq + 1 iterations
    tim->count = 0; // iteration counter
    for(int i=0;i<nt;i++){
        tim->power[i] = 9;
        tim->realpower[i] = 9;
        tim->n_blocks[i] = (int) pow(2.0f,(float) tim->realpower[i]);
        tim->n_threads[i] = dim / tim->n_blocks[i];
    }
    tim->powerid = 0;
    tim->trigger = 0;
    tim->start = clock();
}

void printTimer(timer *tim){
    int nt = tim->ntimers;
    for(int i=0;i< nt ;i++){
        printf("realpower[%d] = %d, ",i,tim->realpower[i]);
    }
    printf("\n");
}

void abandonTiming(timer *tim){
    tim->trigger = 1;
}

void restartTimer(timer *tim){
    tim->trigger = 0;
    tim->count = 0;
    tim->start = clock();
}
