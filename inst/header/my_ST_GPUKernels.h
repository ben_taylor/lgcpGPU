// __global__ void RealToComplex3D(float *invec, cufftComplex *outvec){
//     int idx = threadIdx.x + blockDim.x*blockIdx.x;
//
//     outvec[idx].x = invec[idx];
//     outvec[idx].y = 0.0f; // note that this should be invec[idx].y, but sqare root operation only makes sense for reals anyway, so set imaginary part to zero ...;
// }

/////////////////////////////////////////////////////////////////////////////////////////////////
// Kernels that operate in space AND time
/////////////////////////////////////////////////////////////////////////////////////////////////


__global__ void RealToComplex_3D(float *invec, cufftComplex *outvec, int dim, int stream_idx){
    int idx = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x;

    outvec[idx].x = invec[idx];
    outvec[idx].y = 0.0f; // note that this should be invec[idx].y, but sqare root operation only makes sense for reals anyway, so set imaginary part to zero ...;
}

__global__ void expVec_3D(float *invec, float *outvec, int dim, int stream_idx){
    int idx = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x;
    outvec[idx] = exp(invec[idx]);
}

__global__ void copy_3D(float *from, float *to, int dim, int stream_idx){
    int idx = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x;
    to[idx] = from[idx];
}

__global__ void InitialiseFloat(float *vec, float flt, int dim, int stream_idx){
    int idx = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x;
    vec[idx] = flt;
}


// // Umean <- ((count-1)/count)*Umean + (1/count)*control$U
// // Uvar <- ((count-2)/(count-1))*Uvar + (count/(count-1)^2)*(control$U-Umean)^2
// __global__ void WAICfun(float *running_mean, float *running_var, float *running_mean_exp, float *loglik, int cnt, int dim, int stream_idx){
//     int idx = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x;
//
//     float count = (float) cnt;
//
//     running_mean[idx] = ((count-1)/count)*running_mean[idx] + (1/count)*loglik[idx];
//
//     if(cnt>1){
//         running_var[idx] = ((count-2)/(count-1))*running_var[idx] + (count/((count-1)*(count-1)))*(loglik[idx]-running_mean[idx])*(loglik[idx]-running_mean[idx]);
//     }
//
//     running_mean_exp[idx] = ((count-1)/count)*running_mean_exp[idx] + (1/count)*exp(loglik[idx]);
//     //running_var_exp[idx] = ((count-2)/(count-1))*running_var_exp[idx] + (count/(count-1)^2)*(exp(loglik[idx])-running_mean_exp[idx])^2;
// }

__global__ void Zbeta_3D(float *Zbeta, float *Z, float *beta, int *nbeta, int dim, int stream_idx){
    int i = threadIdx.x + blockDim.x*blockIdx.x;
    int idx, j;
    float ans = 0.0f;
    for(j=0;j<nbeta[0];j++){
        idx = stream_idx*dim*nbeta[0] + i + j*dim;
        ans = ans + Z[idx] * beta[j];
    }
    Zbeta[i + stream_idx*dim] = ans;
}

//__global__ void Eval_e_3D(float *e, float *e_trunc, float *spatial, float *expZbeta, float *Y, float *cellarea, int dim, int stream_idx){
__global__ void Eval_e_3D(float *e, float *spatial, float *expZbeta, float *Y, float *cellarea, int dim, int stream_idx){
    int idx = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x;

    // float EY = exp(Y[idx]);
    // float truncval = EY;
    // float bit = spatial[idx] * expZbeta[idx] * cellarea[idx];
    // if(truncval>50.0f){
    //     truncval = 50.0f;
    // }

    e[idx] = spatial[idx] * expZbeta[idx] * exp(Y[idx]) * cellarea[idx];

    // e[idx] = bit * EY ;
    // e_trunc[idx] = bit * truncval;
}

__global__ void Eval_Intensity_3D(float *e, float *spatial, float *expZbeta, float *Y, int dim, int stream_idx){
    int idx = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x;

    e[idx] = spatial[idx] * expZbeta[idx] * exp(Y[idx]);
}

//__global__ void Eval_NminusE_3D(cufftComplex *NminusE, cufftComplex *NminusE_trunc, float *nis, float *e, float *e_trunc, int dim, int stream_idx){
__global__ void Eval_NminusE_3D(cufftComplex *NminusE, float *nis, float *e, int dim, int stream_idx){
    int idx = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x;

    NminusE[idx].x = nis[idx] - e[idx];
    NminusE[idx].y = 0;

    // NminusE_trunc[idx].x = nis[idx] - e_trunc[idx];
    // NminusE_trunc[idx].y = 0;
}

// logtarget <- -1/2 * sum(gamma[[1]]^2)
// logtarget <- logtarget + sum(sapply(2:nt,function(i){return(sum((-1/(2*(1-bt[i]^2)))*(gamma[[i]]-bt[[i]]*gamma[[i-1]])^2))}))
// logtarget <- logtarget + sum(sapply(1:nt,function(i){sum((Zbeta[[i]]+Y[[i]])*nis[[i]] - e[[i]])}))

//__global__ void Eval_LogLik_3D(float *ll, float *gamma, float *logfactnis, float *Zbeta, float *Y, float *nis, float *e, float* llikrec, float *spatial, float *cellarea, float *bt, int dim, int stream_idx){
__global__ void Eval_LogLik_3D(float *ll, float *gamma, float *Zbeta, float *Y, float *nis, float *e, float *bt, int dim, int stream_idx){
    int idx = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x;
    int idx_minus1 = (stream_idx - 1) * dim + threadIdx.x + blockDim.x*blockIdx.x;

    // float ca = 1.0f;
    // float sp = 1.0f;
    //
    // if(cellarea[idx]>0.0f){
    //     ca = cellarea[idx];
    // }
    // if(spatial[idx]>0.0f){
    //     sp = spatial[idx];
    // }

    float llik;

    llik = (Zbeta[idx] + Y[idx])*nis[idx] - e[idx];

    if(stream_idx == 0){
        ll[idx] =  -(1.0f/2.0f) * gamma[idx]*gamma[idx] + llik;
    }
    else{
        ll[idx] =  -(1.0f/(2.0f*(1.0f-bt[stream_idx]*bt[stream_idx]))) * (gamma[idx] - bt[stream_idx]*gamma[idx_minus1])* (gamma[idx] - bt[stream_idx]*gamma[idx_minus1]) + llik;
    }

}


__global__ void reducePost(float *logtarget_vec, float *loglik,int dim_over_2, int dim, int stream_idx){
    int idx = stream_idx*dim + blockIdx.x; //threadIdx.x + blockDim.x*blockIdx.x;

    if(dim_over_2 == 1){
        logtarget_vec[stream_idx] = loglik[0 + stream_idx*dim] + loglik[1 + stream_idx*dim];
    }
    else{
        loglik[idx] = loglik[idx] + loglik[idx + dim_over_2];
    }
}

__global__ void sumPost(float *logtarget,float *logtarget_vec, float *pri, int numT){
    float ans = pri[0];
    for(int i=0;i<numT;i++){
        ans = ans + logtarget_vec[i];
    }
    logtarget[0] = ans;
}

__global__ void YfromGamma_3D(cufftComplex *toconvert, float *Y, float *eta, float inverse_gridsize, int dim, int stream_idx){
    int idx = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x;

    Y[idx] = -(eta[0] * eta[0])/2.0f + inverse_gridsize * toconvert[idx].x;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// Kernels associated with computing the gradient vector
/////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void EvalGradPrior_beta(float *out, float *beta, float *betaprior_mean, float *betaprior_sd){
    int i = blockIdx.x;

    out[i] = (-1.0f/(betaprior_sd[0]*betaprior_sd[0]))*(beta[i]-betaprior_mean[0]);
}

// note blockDim.x for this kernel is equal to the number of elements in the beta vector
__global__ void EvalGradPrior_beta_setup(float *Zt, float *Zt_working, cufftComplex *NminusE, int dim, int stream_idx){
    // int idx = stream_idx*dim*blockDim.x + threadIdx.x  + blockIdx.x * blockDim.x;
    //
    // Zt_working[idx] =  Zt[idx] * NminusE[stream_idx*dim + blockIdx.x].x;

    int i = threadIdx.x; // row indexd
    int j = blockIdx.x; // column index
    int idx;

    idx = i  + j * blockDim.x;
    Zt_working[stream_idx*dim*blockDim.x + idx] =  Zt[stream_idx*dim*blockDim.x + idx] * NminusE[stream_idx*dim + j].x;
}

// note blockDim.x for this kernel is equal to the number of elements in the beta vector
__global__ void EvalGradPrior_beta_reduce_inblocks(float *out, float *Zt_working, int dim_over_2, int dim, int stream_idx){
    // int i = stream ... these execute in parallel
    int j = blockIdx.x;
    int i = threadIdx.x;
    int idx1, idx2;

    idx1 = stream_idx*dim*blockDim.x + i + j * blockDim.x;
    idx2 = stream_idx*dim*blockDim.x + i + (j+dim_over_2) * blockDim.x;

    if(dim_over_2==1){
        out[stream_idx*blockDim.x + i] = Zt_working[stream_idx*dim*blockDim.x + i] + Zt_working[stream_idx*dim*blockDim.x + i+blockDim.x];
    }
    else{
        Zt_working[idx1] =  Zt_working[idx1] + Zt_working[idx2];
    }
}


// note blockDim.x for this kernel is equal to the number of elements in the beta vector
__global__ void EvalGradPrior_beta_combine(float *out, float *working, int nt){
    int i;

    //out[threadIdx.x] = priorcontrib[threadIdx.x];
    // note, when this function is called, the vector out contains the gradient of beta with respect to the prior
    for(i=0;i<nt;i++){
        out[threadIdx.x] = out[threadIdx.x] + working[i*blockDim.x + threadIdx.x];
    }
}


// if(i==1){
//     grad[[i]] <<- (-1)*gamma[[i]]
// }
// else if(i>1 & i<nt){
//     grad[[i]] <<- -(gamma[[i]] - bt[i]*gamma[[i-1]])/(1-bt[i]^2) + bt[i+1]*(gamma[[i+1]] - bt[i+1]*gamma[[i]])/(1-bt[i+1]^2)
// }
// else{
//     grad[[i]] <<- -(gamma[[i]] - bt[i]*gamma[[i-1]])/(1-bt[i]^2)
// }
__global__ void EvalGradPrior_gamma(float *out, float *gamma, float *bt, int nt, int dim, int stream_idx){
    int idx = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x;

    if(stream_idx==0){
        out[idx] = -gamma[idx];
    }
    else if(stream_idx>0 && stream_idx<(nt-1)){
        out[idx] = -(gamma[idx] - bt[stream_idx]*gamma[(stream_idx-1)*dim + threadIdx.x + blockDim.x*blockIdx.x])/(1.0f-bt[stream_idx]*bt[stream_idx]) + bt[stream_idx+1]*(gamma[(stream_idx+1)*dim + threadIdx.x + blockDim.x*blockIdx.x] - bt[stream_idx+1]*gamma[idx])/(1.0f-bt[stream_idx+1]*bt[stream_idx+1]);
    }
    else{
        out[idx] = -(gamma[idx] - bt[stream_idx]*gamma[(stream_idx-1)*dim + threadIdx.x + blockDim.x*blockIdx.x])/(1.0f-bt[stream_idx]*bt[stream_idx]);
    }
}

// gradcomp[[i]] <<- NminusE[[i]]
// gradmult[[i]] <<- as.list(c(rep(0,i-1),c(1,cumprod(bt[-c(1,1:i)])))) # computes the product of sqrt(gt(i)) * beta(delta t) terms in the summation
// for(i in 1:nt){
//     gradcum <- 0
//     for(j in i:nt){
//         gradcum <- gradcum + gradmult[[i]][[j]] * gradcomp[[j]]
//     }
// }
__global__ void setup_gamma_working(cufftComplex *out,cufftComplex *NminusE,float *bt, int nt, int dim,int stream_idx){
    int idx = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x;
    int j;
    float betaprod = 1.0f;

    out[idx].x = 0.0f;
    out[idx].y = 0.0f;

    //for(j=stream_idx;j<nt;j++){
    for(j=stream_idx;j<nt;j++){
        out[idx].x = out[idx].x + betaprod * NminusE[j*dim + threadIdx.x + blockDim.x*blockIdx.x].x;
        if(j<(nt-1)){
            betaprod = betaprod * bt[j+1]; // should not be not relevant anymore - inserted this conditional statement - returns nonsense when j = nt-1, since bt[(nt-1)+1] = bt[nt] is out of range, but should not affect anything because betaprod is not subsequently used.
        }
    }
}

// note this function assumes that the real matrix is stored as a cufftComplex data type result is stored in complexmat
__global__ void elementwise_Real_multiply_Complex_inplace_3D(cufftComplex *realmat, cufftComplex *complexmat, int dim, int stream_idx){
    int idx = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x;

    complexmat[idx].x = realmat[threadIdx.x + blockDim.x*blockIdx.x].x * complexmat[idx].x;
    complexmat[idx].y = realmat[threadIdx.x + blockDim.x*blockIdx.x].x * complexmat[idx].y;
}

__global__ void EvalGradPrior_gamma_nextstep(float *out, cufftComplex *gamma_working, int dim, int stream_idx){
    int idx = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x;

    out[idx] = out[idx] + (1.0f/((float) dim)) * gamma_working[idx].x;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// Proposal kernels
/////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void getNorms(float *outvec,float *norms, int iteration){
    //int idx = threadIdx.x + blockDim.x*blockIdx.x;
    //outvec[idx] = norms[iteration + idx*totit];
    outvec[threadIdx.x] = norms[(iteration-1)*blockDim.x + threadIdx.x];
}

__global__ void getNorms_3D(float *outvec,float *norms, int iteration, int totit, int nt, int dim, int stream_idx){
    //int idx = threadIdx.x + blockDim.x*blockIdx.x;
    //outvec[idx] = norms[iteration + idx*totit];
    int idx = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x;
    outvec[idx] = norms[(iteration-1)*dim*nt + idx];
}

// propmeans_beta <- betaval + (h/2)*sigma_beta%*%oldtags$gradbeta
__global__ void propBetaMean(float *propbetamean, float *beta, float *h, float *sigma_beta, float *gradbeta, int *nbeta){
    int i = blockIdx.x;
    int j, idx;
    float ans = beta[i];
    for(j=0;j<nbeta[0];j++){
        idx = i + j * nbeta[0];
        ans = ans + (h[0]/2.0f) * sigma_beta[idx] * gradbeta[j];
    }
    propbetamean[i] = ans;
}

// propbeta <- BetaParameters(as.vector(propmeans_beta+sqrt(h)*sigma_beta_chol%*%rnorm(nbeta)))
__global__ void propBetaEta(float *out, float *mean, float *h, float *chol, float *norms, int *npar){
    int i = blockIdx.x;
    int idx, j;
    float ans = mean[i];
    for(j=0;j<npar[0];j++){
        idx = i + j*npar[0];
        ans = ans + sqrt(h[0]) * chol[idx] * norms[j];
    }
    out[i] = ans;
}

// propmeans_gamma <- GP$gamma + (h/2)*gammaVar*oldtags$gradgamma
__global__ void propGammaMean(float *propgammamean, float *gamma, float *h, float *gammaVar, float *gradgamma, int dim, int stream_idx){
    int idx = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x;

    propgammamean[idx] = gamma[idx] + (h[0]/2.0f) * gammaVar[idx] * gradgamma[idx];
}


__global__ void propGamma(float *propgamma, float *propgammamean, float *h, float *sqrtgammaVar, float *gammanorms, int dim, int stream_idx){
    int idx = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x;

    propgamma[idx] = propgammamean[idx] + sqrt(h[0]) * sqrtgammaVar[idx] * gammanorms[idx];
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// Acceptance Ratio Kernels
/////////////////////////////////////////////////////////////////////////////////////////////////

// h <- exp(log(h) + (1/(iteration(mcmcloop)^0.5))*(ac-0.574))
__global__ void h_update(float *d_h, float *d_ac, int i){
    d_h[0] = exp(log(d_h[0]) + (1.0f/sqrt(1.0f * i))*(d_ac[0]-0.574f));
}

__global__ void calculateAcceptance_target(float *d_ac_t, float *d_oldlogtarget, float *d_newlogtarget){

        d_ac_t[0] = d_newlogtarget[0] - d_oldlogtarget[0];

}

__global__ void readyGamma(float *outvec,float *d_ac_gamma, float *d_oldgamma, float *d_newgamma, float *d_propgammamean, float *d_revgammamean, float *d_h, float *d_gammaVar, int dim, int stream_idx){
    int i = stream_idx*dim + threadIdx.x + blockDim.x*blockIdx.x; //blockIdx.x;

    outvec[i] = - (d_oldgamma[i]-d_revgammamean[i])*(d_oldgamma[i]-d_revgammamean[i]) / (2.0f * d_h[0] * d_gammaVar[i]) + (d_newgamma[i]-d_propgammamean[i])*(d_newgamma[i]-d_propgammamean[i]) / (2.0f * d_h[0] * d_gammaVar[i]);

}

__global__ void reduceGamma(float *out_vec, float *vec, int dim_over_2, int dim, int stream_idx){
    int idx = stream_idx*dim + blockIdx.x; // threadIdx.x + blockDim.x*blockIdx.x;

    if(dim_over_2 == 1){
        out_vec[stream_idx] = vec[0 + stream_idx*dim] + vec[1 + stream_idx*dim];
    }
    else{
        vec[idx] = vec[idx] + vec[idx + dim_over_2];
    }
}


__global__ void sumGammaAcc(float *d_ac_gamma,float *d_ac_gamma_vec,int nt){
    int i;
    d_ac_gamma[0] = 0.0f;
    for(i=0;i<nt;i++){
        d_ac_gamma[0] = d_ac_gamma[0] + d_ac_gamma_vec[i];
    }
}



__global__ void calculateAcceptance_beta(float *d_ac_beta, float *d_oldbeta, float *d_newbeta, float *d_propbetamean, float *d_revbetamean, float *d_h, float *d_Q_beta, int *d_nbeta){

    float acc, sum, psum;
    int i, j;

    acc = 0;
    sum = 0.0f;
    for(i=0;i<d_nbeta[0];i++){
        psum = 0.0f;
        for(j=0;j<d_nbeta[0];j++){
            psum = psum + d_Q_beta[i + j*d_nbeta[0]] * (d_oldbeta[j] - d_revbetamean[j]);
        }
        sum = sum + (-1.0f/(2.0f*d_h[0])) * psum * (d_oldbeta[i] - d_revbetamean[i]);
    }
    acc = acc + sum;

    sum = 0.0f;
    for(i=0;i<d_nbeta[0];i++){
        psum = 0.0f;
        for(j=0;j<d_nbeta[0];j++){
            psum = psum + d_Q_beta[i + j*d_nbeta[0]] * (d_newbeta[j] - d_propbetamean[j]);
        }
        sum = sum - (-1.0f/(2.0f*d_h[0])) * psum * (d_newbeta[i] - d_propbetamean[i]);
    }
    acc = acc + sum;

    d_ac_beta[0] = acc;

}

__global__ void combineAcceptance(float *d_ac, float *d_ac_t, float *d_ac_gamma, float *d_ac_beta){
    float logacc = d_ac_t[0] + d_ac_gamma[0] + d_ac_beta[0];
    if(logacc > 0){
        d_ac[0] = 1.0f;
    }
    else{
        d_ac[0] = exp(logacc);
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// Other kernels
/////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void expVec(float *invec, float *outvec){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;
    outvec[idx] = exp(invec[idx]);
}

__global__ void makeBase(float *eta, float *distances, cufftComplex *outmat){ // assumes log eta transformed to eta already
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    outmat[idx].x = eta[0] * eta[0] * exp(- distances[idx] / eta[1]);
    outmat[idx].y = 0;
}

__global__ void SquareRoot_1(cufftComplex *outvec, cufftComplex *invec){ // note order of in an outvec
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    outvec[idx].x = sqrt(invec[idx].x);
    outvec[idx].y = 0.0f; // note that this should be invec[idx].y, but square root operation only makes sense for reals anyway (?), so set imaginary part to zero ...;
}

__global__ void setup_bt(float *bt, float *T, float *expetaomega, int nt){ // note order of in an outvec
    int i;

    // bt <- exp(-exp(omega)*dt)
    bt[0] = 0.0f;
    for(i = 1; i < nt; i++){
        bt[i] = exp(-expetaomega[2] * (T[i] - T[i-1])); // note expetaomega has already been exponentiated, also expetaomega[2] is omega
    }
}

__global__ void EvalPrior(float *out, float *beta, float *etaomega, int *nbeta, float *betaprior_mean, float *betaprior_sd, float *etaprior_mean, float *etaprior_sd, float *omegaprior_mean, float *omegaprior_sd){
    int i;

    float ans = 0.0f;
    float pi = 3.1415926535;

    for(i=0;i<2;i++){
        ans = ans - 0.5f*log(2.0f*pi) - log(etaprior_sd[i]) - (1.0f/(2.0f*etaprior_sd[i]*etaprior_sd[i]))*(etaomega[i] - etaprior_mean[i])*(etaomega[i] - etaprior_mean[i]); // prior for eta
    }
    ans = ans - 0.5f*log(2.0f*pi) - log(omegaprior_sd[0])- (1.0f/(2.0f*omegaprior_sd[0]*omegaprior_sd[0]))*(etaomega[2] - omegaprior_mean[0])*(etaomega[2] - omegaprior_mean[0]); // prior for omega

    for(i=0;i<nbeta[0];i++){
        ans = ans - 0.5f*log(2.0f*pi) - log(betaprior_sd[0]) - (1.0f/(2.0f*betaprior_sd[0]*betaprior_sd[0]))*(beta[i] - betaprior_mean[0])*(beta[i] - betaprior_mean[0]); // prior for beta
    }
    out[0] = ans;
}
