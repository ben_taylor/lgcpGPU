//#include <cufft.h>
//#include <helper_functions.h>
//#include <helper_cuda.h>

////////////////////////////////////////////////////////////////////////
// function declarations
////////////////////////////////////////////////////////////////////////

void printArray(float *array, int nrow, int ncol, int depth);
void printMatrix(float *matrix, int nrow, int ncol);
void printMatrix_double(double *matrix, int nrow, int ncol);
void printMatrix_cufftComplex(cufftComplex *matrix, int nrow, int ncol);
//void printMatrix_complex(fftw_complex *matrix, int nrow, int ncol);
int ijToIndex(int i, int j, int ncol);
int ijToIndex_bycol(int i, int j, int nrow);
void matrixToVector_bycolumn(float *matrix, int nrow, int ncol,
                             float *vector, int length);
void pmin_float(float *x1, float *x2, float *minvec, int length);
void pmin_int(int *x1, int *x2, int *minvec, int length);
void Re(cufftComplex *inmatrix, float *outmatrix, int length);
void checkFloat(float *obj,int nx, int ny);
void checkInt(int *obj);
void checkcufftComplex(cufftComplex *obj,int nx, int ny);

////////////////////////////////////////////////////////////////////////
// function definitions
////////////////////////////////////////////////////////////////////////


void matrixToVector_bycolumn(float *matrix, int nrow, int ncol,
                             float *vector, int length){

    int i, j;

     if(length!=(nrow*ncol)){
         printf("Mismatch between size of output vector and matrix dimension.");
         exit(1);
     }

    int count = 0;
    for(j=0;j<ncol;j++){
        for(i=0;i<nrow;i++){
            vector[count] = matrix[ijToIndex(i,j,ncol)];
            count = count + 1;
        }
    }

}



void printArray(float *array, int nrow, int ncol, int depth){

    int i, j, k, ind;

    for(k=0; k<depth; k++){
        for (i=0; i<nrow; i++){
            for (j=0; j<ncol; j++){
                ind = i + j*ncol + k*(nrow * ncol); //ijToIndex(i,j,nrow);//i + j*ncol;
                if(j==ncol-1){
                    printf("%.9f \n",array[ind]);
                }
                else{
                    printf("%.9f ",array[ind]);
                }
            }
        }
    }
    printf("\n\n");
}



void printMatrix(float *matrix, int nrow, int ncol){

    int i, j, ind;

    for (i=0; i<nrow; i++){
        for (j=0; j<ncol; j++){
            ind = ijToIndex(i,j,nrow);//i + j*ncol;
            if(j==ncol-1){
                printf("%.9f \n",matrix[ind]);
            }
            else{
                printf("%.9f ",matrix[ind]);
            }
        }
    }
    printf("\n\n");
}



void printMatrix_double(double *matrix, int nrow, int ncol){

    int i, j, ind;

    for (i=0; i<nrow; i++){
        for (j=0; j<ncol; j++){
            ind = ijToIndex(i,j,nrow);//i + j*ncol;
            if(j==ncol-1){
                printf("%.9g \n",matrix[ind]);
            }
            else{
                printf("%.9g ",matrix[ind]);
            }
        }
    }
    printf("\n\n");
}



void printMatrix_cufftComplex(cufftComplex *matrix, int nrow, int ncol){

    int i, j, ind;

    printf("Real Part:\n\n");
    for (i=0; i<nrow; i++){
        for (j=0; j<ncol; j++){
            ind = ijToIndex(i,j,nrow);//i + j*ncol;
            if(j==ncol-1){
                printf("%.9g \n",matrix[ind].x);
            }
            else{
                printf("%.9g ",matrix[ind].x);
            }
        }
    }
    printf("Imaginary Part:\n\n");
    for (i=0; i<nrow; i++){
        for (j=0; j<ncol; j++){
            ind = ijToIndex(i,j,nrow);//i + j*ncol;
            if(j==ncol-1){
                printf("%.9g \n",matrix[ind].y);
            }
            else{
                printf("%.9g ",matrix[ind].y);
            }
        }
    }
    printf("\n\n");
}



void checkFloat(float *obj,int nx, int ny){
    float *temp;
    temp = (float*)malloc(nx * ny * sizeof(float));
    //checkCudaErrors( cudaMemcpy(temp,obj,nx * ny * sizeof(float),cudaMemcpyDeviceToHost) );
    cudaMemcpy(temp,obj,nx * ny * sizeof(float),cudaMemcpyDeviceToHost); // no checking ...
    printMatrix(temp,nx,ny);
    free(temp);
}



void checkInt(int *obj){
    int *temp;
    temp = (int*)malloc(sizeof(int));
    //checkCudaErrors( cudaMemcpy(temp,obj,sizeof(int),cudaMemcpyDeviceToHost) );
    cudaMemcpy(temp,obj,sizeof(int),cudaMemcpyDeviceToHost);
    printf("%d\n",*temp);
    free(temp);
}



void checkcufftComplex(cufftComplex *obj,int nx, int ny){
    cufftComplex *temp;
    temp = (cufftComplex*)malloc(nx * ny * sizeof(cufftComplex));
    //checkCudaErrors( cudaMemcpy(temp,obj,nx * ny * sizeof(cufftComplex),cudaMemcpyDeviceToHost) );
    cudaMemcpy(temp,obj,nx * ny * sizeof(cufftComplex),cudaMemcpyDeviceToHost);
    printMatrix_cufftComplex(temp,nx,ny);
    free(temp);
}


/*
void printMatrix_complex(fftw_complex *matrix, int nrow, int ncol){

    int i, j, ind;

    for (i=0; i<nrow; i++){
        for (j=0; j<ncol; j++){
            ind = ijToIndex(i,j,nrow);//i + j*ncol;
            if(j==ncol-1){
                //printf("%3.7f \n",matrix[ind]);
                printf("%3.4f + %3.4f i\n", matrix[ind][0], matrix[ind][1]);
            }
            else{
                //printf("%3.7f ",matrix[ind]);
                printf("%3.4f + %3.4f i  ", matrix[ind][0], matrix[ind][1]);
            }
        }
    }
}
*/



int ijToIndex(int i, int j, int ncol){
    int ans = i + j*ncol;
    return ans;
}



int ijToIndex_bycol(int i, int j, int nrow){
    int ans = i*nrow + j;
    return ans;
}



void pmin_float(float *x1, float *x2, float *minvec, int length){ // all inputs must have the same length
    int i;
    for (i=0;i<length;i++){
        if(x1[i]<x2[i]){
            minvec[i] = x1[i];
        }
        else{
            minvec[i] = x2[i];
        }
    }
}



void pmin_int(int *x1, int *x2, int *minvec, int length){ // all inputs must have the same length
    int i;
    for (i=0;i<length;i++){
        if(x1[i]<x2[i]){
            minvec[i] = x1[i];
        }
        else{
            minvec[i] = x2[i];
        }
    }
}






/*
void printMatrix_byrow(float *matrix, int nrow, int ncol){

    int i, j, ind;

    for (i=0; i<nrow; i++){
        for (j=0; j<ncol; j++){
            ind = ijToIndex(i,j,ncol);//i + j*ncol;
            if(j==ncol-1){
                printf("%3.7f \n",matrix[ind]);
            }
            else{
                printf("%3.7f ",matrix[ind]);
            }
        }
    }
}
*/
