////////////////////////////////////////////////////////////////////////
// function declarations
////////////////////////////////////////////////////////////////////////

void circulantVector(float *in_vector,  int in_length,
                     float *out_vector, int out_length);
//void circulantMatrix(float *in_matrix,  int in_nrow,  int in_ncol,
//                     float *out_matrix, int out_nrow, int out_ncol);
void blockcircbase(float *x, int M, float *y, int N, float sigma, float phi, float*outmat, int outlength);

////////////////////////////////////////////////////////////////////////
// function definitions
////////////////////////////////////////////////////////////////////////

void blockcircbase(float *x, int M, float *y, int N, float sigma, float phi, float *outmat, int outlength){

    float dx, dy;//, *d;
    int i, *xidx, *M_minus_xidx, *yidx, *N_minus_yidx, *dxidx, *dyidx;//, count;

    dx = 1.0f*(x[1] - x[0]);
    dy = 1.0f*(y[1] - y[0]);

    xidx = (int *)malloc(sizeof(int)*M*N);
    M_minus_xidx = (int *)malloc(sizeof(int)*M*N);
    yidx = (int *)malloc(sizeof(int)*M*N);
    N_minus_yidx = (int *)malloc(sizeof(int)*M*N);
    dxidx = (int *)malloc(sizeof(int)*M*N);
    dyidx = (int *)malloc(sizeof(int)*M*N);
    //d = (float *)malloc(sizeof(float)*M*N);

    for(i=0; i<M*N; i++){
        xidx[i] = (i%M);
        yidx[i] = (i/M);

        M_minus_xidx[i] = M - xidx[i];
        N_minus_yidx[i] = N - yidx[i];
    }

    /*
    for(i=0; i<M*N; i++){
        printf("xidx[%d] = %d\n",i,xidx[i]);
    }
    for(i=0; i<M*N; i++){
        printf("yidx[%d] = %d\n",i,yidx[i]);
    }
    */

    //R code: dxidx <- pmin(abs(xidx - xidx[1]), M - abs(xidx - xidx[1]))
    //R code: dyidx <- pmin(abs(yidx - yidx[1]), N - abs(yidx - yidx[1]))

    pmin_int(xidx,M_minus_xidx, dxidx, M*N); // see R code below: the R code "xidx - xidx[1]" in C is just xidx, since xidx[1] = 0, abs is also not necessary
    pmin_int(yidx,N_minus_yidx, dyidx, M*N);

    for(i=0; i<M*N; i++){
        outmat[i] = sigma*sigma*exp(-1.0f*sqrt((dx * dxidx[i])*(dx * dxidx[i]) + (dy * dyidx[i])*(dy * dyidx[i])) / phi);
    }

    /*
    count = 0;
    for(i=0; i<M; i++){
        for(j=0; j<N; j++){
            outmat[ijToIndex_bycol(i,j,M)] = d[count];
            count = count + 1;
        }
    }
    */

    /*
    for(i=0; i<M*N; i++){
        printf("dxidx[%d] = %d\n",i,dxidx[i]);
    }
    for(i=0; i<M*N; i++){
        printf("dyidx[%d] = %d\n",i,dyidx[i]);
    }
    for(i=0; i<M*N; i++){
        printf("outmat[%d] = %3.3f\n",i,outmat[i]);
    }
    */




    free(xidx);
    free(M_minus_xidx);
    free(yidx);
    free(N_minus_yidx);
    free(dxidx);
    free(dyidx);
    //free(d);

    //free(outidx);

    /*
    xidx <- rep(1:M, N)
    yidx <- rep(1:N, each = M)
    dxidx <- pmin(abs(xidx - xidx[1]), M - abs(xidx - xidx[1]))
    dyidx <- pmin(abs(yidx - yidx[1]), N - abs(yidx - yidx[1]))
    d <- sqrt(((x[2] - x[1]) * dxidx)^2 + ((y[2] - y[1]) * dyidx)^2)
    covbase <- matrix(gu(d, sigma = sigma, phi = phi, model = model,
        additionalparameters = additionalparameters), M, N)
    if (!inverse) {
        return(covbase)
    }
    else {
        return(inversebase(covbase))
    }
    */




}



void circulantVector(float *in_vector,  int in_length,
                     float *out_matrix, int out_length){

    int i, j, *idx, *idxcopy;

    if(out_length!=(in_length*in_length)){
        printf("Mismatch between size of output matrix and base dimension.");
        exit(1);
    }

    idx = (int *)malloc(sizeof(int)*in_length);
    idxcopy = (int *)malloc(sizeof(int)*in_length);

    for(i=0;i<in_length;i++){
        idx[i] = i;
    }

    for(i=0;i<in_length;i++){
        for(j=0;j<in_length;j++){
            out_matrix[ijToIndex(i,j,in_length)] = in_vector[idx[j]];
        }

        // update index ... probably can be done more efficiently ...
        // take the last element off and stick it on the beginning of the vector
        idxcopy[0] = idx[in_length-1];
        for(j=1;j<in_length;j++){
            idxcopy[j] = idx[j-1];
        }
        for(j=0;j<in_length;j++){
            idx[j] = idxcopy[j];
        }
    }

    free(idx);
    free(idxcopy);

}


/*
void circulantMatrix(float *in_matrix,  int in_nrow,  int in_ncol,
                     float *out_matrix, int out_nrow, int out_ncol){
    int M = in_row;
    int N = in_col;
    float *vec, *tempmat, *outvec;

    float *submats;

    submats = (float *)malloc(sizeof(float)*M*N); // want output of this to be M x N
    vec = (float *)malloc(sizeof(float)*M); // want output of this to be M x N
    tempmat = (float *)malloc(sizeof(float)*M*M);
    outvec = (float *)malloc(sizeof(float)*M*M);

    for(j=0;j<N;j++){
        for(i=0;i<M;i++){
            vec[i] = in_matrix[ijToIndex(i,j,in_ncol)];
        }
        circulantVector(vec,M,tempmat,M*M)
        matrixToVector_bycolumn(tempmat,M,M,outvec, M*M)
        submats[ijToIndex(i,j,)]
    }

    submats <- t(apply(x,2,circulant))
    mat <- matrix(NA,M*N,M*N)
    idx <- 1:N
    ct <- 1
    for (i in 1:N){
        for (j in 1:N){
            xstart <- M*floor((ct-1)/N) + 1
            mult <- ct%%N
            if (mult==0){
                mult <- N
            }
            ystart <- M*(mult-1) + 1
            mat[xstart:(xstart+M-1),ystart:(ystart+M-1)] <- submats[idx[j],]
            ct <- ct + 1
        }
        idx <- c(idx[N],idx[-N])
    }
    return(mat)

    free(submats);
    free(vec);
    free(tempmat);
    free(outvec);
}
*/
