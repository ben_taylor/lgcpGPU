// #include <hdf5/serial/hdf5.h>
// #include <hdf5/serial/H5Dpublic.h>

////////////////////////////////////////////////////////////////////////
// function declarations
////////////////////////////////////////////////////////////////////////

void readHDF5int(const char *fname, const char *oname, int *A);
void readHDF5float(const char *fname, const char *oname, float *A);
void readHDF5char(const char *fname, const char *oname, char *A);

////////////////////////////////////////////////////////////////////////
// function definitions
////////////////////////////////////////////////////////////////////////



void readHDF5int(const char *fname, const char *oname,int *A){

    // (substantially) modified from www.math-cs.gordon.edu/courses/cps343/presentations/HDF5.pdf
    hid_t file_id, dataset_id; //, dataspace_id, file_dataspace_id;

    file_id = H5Fopen(fname,H5F_ACC_RDONLY,H5P_DEFAULT);
    /*Openexistingdataset*/
    dataset_id = H5Dopen(file_id,oname,H5P_DEFAULT);

    /*Readmatrixdatafromfile*/
    // SET DATA TYPE IN THIS LINE !!!!!
    H5Dread (dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL,H5P_DEFAULT, A);

    /*Releaseresourcesandclosefile*/
    H5Dclose(dataset_id);
    H5Fclose(file_id);

}

void readHDF5float(const char *fname, const char *oname,float *A){

    // (substantially) modified from www.math-cs.gordon.edu/courses/cps343/presentations/HDF5.pdf
    hid_t file_id, dataset_id; //, dataspace_id, file_dataspace_id;

    file_id = H5Fopen(fname,H5F_ACC_RDONLY,H5P_DEFAULT);
    /*Openexistingdataset*/
    dataset_id = H5Dopen(file_id,oname,H5P_DEFAULT);

    /*Readmatrixdatafromfile*/
    // SET DATA TYPE IN THIS LINE !!!!!
    H5Dread (dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL,H5P_DEFAULT, A);

    /*Releaseresourcesandclosefile*/
    H5Dclose(dataset_id);
    H5Fclose(file_id);

}

void readHDF5char(const char *fname, const char *oname, char *A){

    // (substantially) modified from www.math-cs.gordon.edu/courses/cps343/presentations/HDF5.pdf
    hid_t file_id, dataset_id; //, dataspace_id, file_dataspace_id;

    file_id = H5Fopen(fname,H5F_ACC_RDONLY,H5P_DEFAULT);
    /*Openexistingdataset*/
    dataset_id = H5Dopen(file_id,oname,H5P_DEFAULT);

    /*Readmatrixdatafromfile*/
    // SET DATA TYPE IN THIS LINE !!!!!
    //H5Dread (dataset_id, H5T_NATIVE_CHAR, H5S_ALL, H5S_ALL,H5P_DEFAULT, A);
    H5Dread (dataset_id, H5T_C_S1, H5S_ALL, H5S_ALL,H5P_DEFAULT, A);

    /*Releaseresourcesandclosefile*/
    H5Dclose(dataset_id);
    H5Fclose(file_id);

}

//
// void readHDF5Int(char *fname, char *oname,int *A){
//     // modified from www.math-cs.gordon.edu/courses/cps343/presentations/HDF5.pdf
//     hid_t file_id, dataset_id, dataspace_id, file_dataspace_id;
//     hsize_t *dims;
//     //hssize_t num_elem;
//     //herr_t status;
//     int rank;
//     int ndims;
//     //int rows, cols, stride;
//
//     //int *A;
//
//     /*OpenexistingHDF5file*/
//     file_id = H5Fopen(fname,H5F_ACC_RDONLY,H5P_DEFAULT);
//     /*Openexistingdataset*/
//     dataset_id = H5Dopen(file_id,oname,H5P_DEFAULT);
//
//     /*Determinedatasetparameters*/
//     file_dataspace_id = H5Dget_space(dataset_id);
//     rank = H5Sget_simple_extent_ndims(file_dataspace_id);
//     dims = (hsize_t*)malloc(rank*sizeof(hsize_t));
//     ndims = H5Sget_simple_extent_dims(file_dataspace_id,dims,NULL);
//     if(ndims!=rank){
//         fprintf(stderr,"Expected data space to be dimension");
//         fprintf(stderr,"%d but appears to be %d\n",rank,ndims);
//     }
//     /*Allocatematrix*/
//     //num_elem = H5Sget_simple_extent_npoints(file_dataspace_id);
//
//     //A = (int*)malloc(num_elem*sizeof(int));
//
//     //rows = dims[0];
//     //cols = dims[1];
//     //stride = cols;
//     free(dims);
//
//     /*Createdataspace*/
//     dataspace_id = H5Screate_simple(rank,dims,NULL);
//
//     /*Readmatrixdatafromfile*/
//     // SET DATA TYPE IN THIS LINE !!!!!
//     //H5Dread(dataset_id,H5T_NATIVE_INT,dataspace_id,file_dataspace_id,H5P_DEFAULT,A);
//     H5Dread (dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL,H5P_DEFAULT, A);
//
//     /*Releaseresourcesandclosefile*/
//     H5Dclose(dataset_id);
//     H5Sclose(dataspace_id);
//     H5Sclose(file_dataspace_id);
//     H5Fclose(file_id);
//
// }
