///////////////////////////////////////////////////////////////////////////////
// GPU Kernels
///////////////////////////////////////////////////////////////////////////////

void Re(cufftComplex *mat, int length){
    int i;
    for(i=0;i<length;i++){
        mat[i].y = 0.0f;
    }
}

__global__ void SquareRoot(cufftComplex *invec, cufftComplex *outvec, int length){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;
    if(idx<length){
        outvec[idx].x = sqrt(invec[idx].x);
        outvec[idx].y = 0.0f; // note that this should be invec[idx].y, but square root operation only makes sense for reals anyway (?), so set imaginary part to zero ...;
    }
}

__global__ void SquareRoot_1(cufftComplex *outvec, cufftComplex *invec){ // note order of in an outvec
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    outvec[idx].x = sqrt(invec[idx].x);
    outvec[idx].y = 0.0f; // note that this should be invec[idx].y, but square root operation only makes sense for reals anyway (?), so set imaginary part to zero ...;
}

__global__ void RealToComplex(float *invec, cufftComplex *outvec, int length){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;
    if(idx<length){
        outvec[idx].x = invec[idx];
        outvec[idx].y = 0.0f; // note that this should be invec[idx].y, but sqare root operation only makes sense for reals anyway, so set imaginary part to zero ...;
    }
}

__global__ void RealToComplex_1(float *invec, cufftComplex *outvec){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    outvec[idx].x = invec[idx];
    outvec[idx].y = 0.0f; // note that this should be invec[idx].y, but sqare root operation only makes sense for reals anyway, so set imaginary part to zero ...;
}

// note this function assumes that the real matrix is stored as a cufftComplex data type result is stored in complexmat
__global__ void elementwise_Real_multiply_Complex_inplace(cufftComplex *realmat, cufftComplex *complexmat, int length){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;
    if(idx<length){
        complexmat[idx].x = realmat[idx].x * complexmat[idx].x;
        complexmat[idx].y = realmat[idx].x * complexmat[idx].y;
    }
}

// note this function assumes that the real matrix is stored as a cufftComplex data type result is stored in complexmat
__global__ void elementwise_Real_multiply_Complex_inplace_1(cufftComplex *realmat, cufftComplex *complexmat){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    complexmat[idx].x = realmat[idx].x * complexmat[idx].x;
    complexmat[idx].y = realmat[idx].x * complexmat[idx].y;
}

__global__ void YfromGamma(cufftComplex *toconvert, float *Y, float mu, float inverse_gridsize, int length){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;
    if(idx<length){
        Y[idx] = mu + inverse_gridsize * toconvert[idx].x;
    }
}

__global__ void YfromGamma_1(cufftComplex *toconvert, float *Y, float *eta, float inverse_gridsize){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    Y[idx] = -(eta[0] * eta[0])/2 + inverse_gridsize * toconvert[idx].x;
}

///////////////////////////////////////////////////////////////////////////////
// GPU Kernels
///////////////////////////////////////////////////////////////////////////////

__global__ void getNorms(float *outvec,float* norms, int iteration, int totit){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    outvec[idx] = norms[iteration + idx*totit];
}

__global__ void expVec(float *invec, float *outvec){
    //outvec[threadIdx.x] = exp(invec[threadIdx.x]);
    int idx = threadIdx.x + blockDim.x*blockIdx.x;
    outvec[idx] = exp(invec[idx]);
}

__global__ void makeBase(float *eta, float *distances, cufftComplex *outmat){ // assumes log eta transformed to eta already
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    outmat[idx].x = eta[0] * eta[0] * exp(- distances[idx] / eta[1]);
    outmat[idx].y = 0;
}

// propmeans_beta <- betaval + (h/2)*sigma_beta%*%oldtags$gradbeta
__global__ void propBetaMean(float *propbetamean, float *beta, float *h, float *sigma_beta, float *gradbeta, int *nbeta){
    int i = blockIdx.x;
    int j, idx;
    float ans = beta[i];
    for(j=0;j<nbeta[0];j++){
        idx = i + j * nbeta[0];
        ans = ans + (h[0]/2) * sigma_beta[idx] * gradbeta[j];
    }
    propbetamean[i] = ans;
}

// propbeta <- BetaParameters(as.vector(propmeans_beta+sqrt(h)*sigma_beta_chol%*%rnorm(nbeta)))
__global__ void propBetaEta(float *out, float *mean, float *h, float *chol, float *norms, int *npar){
    int i = blockIdx.x;
    int idx, j;
    float ans = mean[i];
    for(j=0;j<npar[0];j++){
        idx = i + j*npar[0];
        ans = ans + sqrt(h[0]) * chol[idx] * norms[j];
    }
    out[i] = ans;
}



// propmeans_gamma <- GP$gamma + (h/2)*gammaVar*oldtags$gradgamma
__global__ void propGammaMean(float *propgammamean, float *gamma, float *h, float *gammaVar, float *gradgamma){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    propgammamean[idx] = gamma[idx] + (h[0]/2) * gammaVar[idx] * gradgamma[idx];
}


__global__ void propGamma(float *propgamma, float *propgammamean, float *h, float *sqrtgammaVar, float *gammanorms){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    propgamma[idx] = propgammamean[idx] + sqrt(h[0]) * sqrtgammaVar[idx] * gammanorms[idx];
}


__global__ void Zbeta(float *Zbeta, float *Z, float *beta, int *nbeta, int dim){
    int i = threadIdx.x + blockDim.x*blockIdx.x;
    int idx, j;
    float ans = 0.0f;
    for(j=0;j<nbeta[0];j++){
        idx = i + j*dim;
        ans = ans + Z[idx] * beta[j];
    }
    Zbeta[i] = ans;
}


__global__ void EvalPrior(float *out, float *beta, float *eta, int *nbeta, int *neta, float *betaprior_mean, float *betaprior_variance, float *etaprior_mean, float *etaprior_variance){
    int i;
    float ans = 0.0f;
    for(i=0;i<neta[0];i++){
        ans = ans - (1.0f/(2*etaprior_variance[i]))*(eta[i] - etaprior_mean[i])*(eta[i] - etaprior_mean[i]);
    }

    for(i=0;i<nbeta[0];i++){
        ans = ans - (1.0f/(2*betaprior_variance[i]))*(beta[i] - betaprior_mean[i])*(beta[i] - betaprior_mean[i]);
    }
    out[0] = ans;
}



// __global__ void setVal(float *out, float val){
//     int idx = threadIdx.x + blockDim.x*blockIdx.x;
//
//     out[idx] = val;
// }

__global__ void Eval_e(float *e, float *spatial, float *expZbeta, float *Y, float *cellarea){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    //spatial*expZbeta*GP$expY*cellarea
    e[idx] = spatial[idx] * expZbeta[idx] * exp(Y[idx]) * cellarea[0];
}

__global__ void Eval_NminusE(cufftComplex *NminusE, float *nis, float *e){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    NminusE[idx].x = nis[idx] - e[idx];
    NminusE[idx].y = 0;
}

__global__ void Eval_LogLik(float *ll, float *gamma, float *Zbeta, float *Y, float *nis, float *e){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    ll[idx] = -(1.0f/2) * gamma[idx]*gamma[idx] + (Zbeta[idx]+Y[idx])*nis[idx] - e[idx];
}

__global__ void Eval_Post(float *ans, float *ll, float *pri, int dim){
    int i;
    ans[0] = pri[0];
    for(i=0;i<dim;i++){
        ans[0] = ans[0] + ll[i];
    }
}

__global__ void reducePost(float *logtarget, float *loglik, float *pri,int dim_over_2){
    int idx = blockIdx.x; //threadIdx.x + blockDim.x*blockIdx.x;

    if(dim_over_2 == 1){
        logtarget[0] = loglik[0] + loglik[1] + pri[0];
    }
    else{
        loglik[idx] = loglik[idx] + loglik[idx + dim_over_2];
    }
}


__global__ void EvalGradPrior_beta(float *out, float *beta, float *betaprior_mean, float *betaprior_variance){
    int i = blockIdx.x;

    out[i] = (-1.0f/(betaprior_variance[i]))*(beta[i]-betaprior_mean[i]);
}

__global__ void EvalGradPrior_beta_nextstep(float *out, float *Zt, cufftComplex *NminusE, int dim, int *nbeta){
    int i = blockIdx.x;
    int j, idx;

    float sum = 0.0f;
    for(j=0;j<dim;j++){
        idx = i  + j * nbeta[0];
        sum = sum + Zt[idx] * NminusE[j].x;
    }

    out[i] = out[i] + sum;
}

__global__ void EvalGradPrior_beta_setup(float *Zt, float *Zt_working, cufftComplex *NminusE, int *nbeta){
    int i = threadIdx.x; // row indexd
    int j = blockIdx.x; // column index
    int idx;

    idx = i  + j * nbeta[0];
    Zt_working[idx] =  Zt[idx] * NminusE[j].x;
}

__global__ void EvalGradPrior_beta_reduce(float *out, float *Zt_working, int*nbeta, int dim_over_2, int i){
    // int i = stream ... these execute in parallel
    int j = blockIdx.x;
    int idx1, idx2;

    idx1 = i + j * nbeta[0];
    idx2 = i + (j+dim_over_2) * nbeta[0];

    if(dim_over_2==1){
        out[i] =  out[i] + Zt_working[i] + Zt_working[i+nbeta[0]];
    }
    else{
        Zt_working[idx1] =  Zt_working[idx1] + Zt_working[idx2];
    }
}

__global__ void EvalGradPrior_beta_reduce_inblocks(float *out, float *Zt_working, int*nbeta, int dim_over_2){
    // int i = stream ... these execute in parallel
    int j = blockIdx.x;
    int i = threadIdx.x;
    int idx1, idx2;

    idx1 = i + j * nbeta[0];
    idx2 = i + (j+dim_over_2) * nbeta[0];

    if(dim_over_2==1){
        out[i] =  out[i] + Zt_working[i] + Zt_working[i+nbeta[0]];
    }
    else{
        Zt_working[idx1] =  Zt_working[idx1] + Zt_working[idx2];
    }
}

__global__ void EvalGradPrior_betaTemp(float *out, float *Zt_working, int*nbeta, int dim){
    // int i = stream ... these execute in parallel
    int i = blockIdx.x;
    int j, idx;

    float sum = 0.0f;
    for(j=0;j<dim;j++){
        idx = i + j * nbeta[0];
        sum = sum + Zt_working[idx];
    }

    out[i] = sum;
}



__global__ void EvalGradPrior_gamma(float *out, float *gamma){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    out[idx] = -gamma[idx];
}

__global__ void EvalGradPrior_gamma_nextstep(float *out, cufftComplex *NminusE,int dim){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    out[idx] = out[idx] + (1.0f/dim) * NminusE[idx].x;
}


// ac <- exp(proptags$logtarget-oldtags$logtarget
//                     -sum((GP$gamma-revpropmeans_gamma)^2/(2*h*gammaVar))
//                     + sum((propGP$gamma-propmeans_gamma)^2/(2*h*gammaVar))
//                     + (-(0.5/h)*t(betaval-revpropmeans_beta)%*%Q_beta%*%(betaval-revpropmeans_beta))
//                     - (-(0.5/h)*t(propbeta-propmeans_beta)%*%Q_beta%*%(propbeta-propmeans_beta)))
__global__ void calculateAcceptance_target(float *d_ac_t, float *d_oldlogtarget, float *d_newlogtarget){

        d_ac_t[0] = d_newlogtarget[0] - d_oldlogtarget[0];

}

__global__ void calculateAcceptance_gamma(float *d_ac_gamma, float *d_oldgamma, float *d_newgamma, float *d_propgammamean, float *d_revgammamean, float *d_h, float *d_gammaVar, int dim){

    float acc;
    int i;

    acc = 0.0f;
    for(i=0;i<dim;i++){
        acc = acc - (d_oldgamma[i]-d_revgammamean[i])*(d_oldgamma[i]-d_revgammamean[i]) / (2 * d_h[0] * d_gammaVar[i]);
        acc = acc + (d_newgamma[i]-d_propgammamean[i])*(d_newgamma[i]-d_propgammamean[i]) / (2 * d_h[0] * d_gammaVar[i]);
    }
    d_ac_gamma[0] = acc;


}

__global__ void readyGamma(float *outvec,float *d_ac_gamma, float *d_oldgamma, float *d_newgamma, float *d_propgammamean, float *d_revgammamean, float *d_h, float *d_gammaVar, int dim){
    int i = threadIdx.x + blockDim.x*blockIdx.x; //blockIdx.x;

    outvec[i] = - (d_oldgamma[i]-d_revgammamean[i])*(d_oldgamma[i]-d_revgammamean[i]) / (2 * d_h[0] * d_gammaVar[i]) + (d_newgamma[i]-d_propgammamean[i])*(d_newgamma[i]-d_propgammamean[i]) / (2 * d_h[0] * d_gammaVar[i]);

}

__global__ void reduceGamma(float *out, float *vec, int dim_over_2){
    int idx = blockIdx.x; // threadIdx.x + blockDim.x*blockIdx.x;

    if(dim_over_2 == 1){
        out[0] = vec[0] + vec[1];
    }
    else{
        vec[idx] = vec[idx] + vec[idx + dim_over_2];
    }
}

__global__ void reduceGamma_naive(float *out, float *vec, int dim){
    float sum = 0.0f;
    for(int i =0;i<dim;i++){
        sum = sum + vec[i];
    }
    out[0] = sum;
}

__global__ void calculateAcceptance_beta(float *d_ac_beta, float *d_oldbeta, float *d_newbeta, float *d_propbetamean, float *d_revbetamean, float *d_h, float *d_Q_beta, int *d_nbeta){

    float acc, sum, psum;
    int i, j;

    acc = 0;
    sum = 0.0f;
    for(i=0;i<d_nbeta[0];i++){
        psum = 0.0f;
        for(j=0;j<d_nbeta[0];j++){
            psum = psum + d_Q_beta[i + j*d_nbeta[0]] * (d_oldbeta[j] - d_revbetamean[j]);
        }
        sum = sum + (-1.0f/(2*d_h[0])) * psum * (d_oldbeta[i] - d_revbetamean[i]);
    }
    acc = acc + sum;

    sum = 0.0f;
    for(i=0;i<d_nbeta[0];i++){
        psum = 0.0f;
        for(j=0;j<d_nbeta[0];j++){
            psum = psum + d_Q_beta[i + j*d_nbeta[0]] * (d_newbeta[j] - d_propbetamean[j]);
        }
        sum = sum - (-1.0f/(2*d_h[0])) * psum * (d_newbeta[i] - d_propbetamean[i]);
    }
    acc = acc + sum;

    d_ac_beta[0] = acc;

}

__global__ void combineAcceptance(float *d_ac, float *d_ac_t, float *d_ac_gamma, float *d_ac_beta){
    float logacc = d_ac_t[0] + d_ac_gamma[0] + d_ac_beta[0];
    if(logacc > 0){
        d_ac[0] = 1.0f;
    }
    else{
        d_ac[0] = exp(logacc);
    }
}


// h <- exp(log(h) + (1/(iteration(mcmcloop)^0.5))*(ac-0.574))
__global__ void h_update(float *d_h, float *d_ac, int i){
    d_h[0] = exp(log(d_h[0]) + (1.0f/sqrt(1.0f * i))*(d_ac[0]-0.574f));
}

__global__ void updateCurrent(float *outvec, float *invec){
    int idx = threadIdx.x + blockDim.x*blockIdx.x;

    outvec[idx] = invec[idx];
}

// __global__ void reduce_array(float *g_odata, float *g_idata, unsigned int n){
//
//     __shared__ float *sdata;
//
//     // load shared mem
//     unsigned int tid = threadIdx.x;
//     unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
//
//     sdata[tid] = (i < n) ? g_idata[i] : 0;
//
//     __syncthreads();
//
//     // do reduction in shared mem
//     for (unsigned int s=blockDim.x/2; s>0; s>>=1)
//     {
//         if (tid < s)
//         {
//             sdata[tid] += sdata[tid + s];
//         }
//
//         __syncthreads();
//     }
//
//     // write result for this block to global mem
//     if (tid == 0) g_odata[blockIdx.x] = sdata[0];
// }

































// __global__ void MatMul_wide(float *out, float *mat, float *vec, const int NX, const int NY){
//     int tid=threadIdx.x+blockIdx.x*blockDim.x;
//         float sum=0;
//     if(tid<NY){
//         for(int i=0; i<NX; i++)
//             sum += vec[i]*mat[(i*NY)+tid];
//         out[tid]=sum;
//     }
// }


// template <int BLOCK_SIZE> __global__ void matrixMulCUDA(float *C, float *A, float *B, int wA, int wB){
//
//     //int BLOCK_SIZE = 32;
//
//     // Block index
//     int bx = blockIdx.x;
//     int by = blockIdx.y;
//
//     // Thread index
//     int tx = threadIdx.x;
//     int ty = threadIdx.y;
//
//     // Index of the first sub-matrix of A processed by the block
//     int aBegin = wA * BLOCK_SIZE * by;
//
//     // Index of the last sub-matrix of A processed by the block
//     int aEnd   = aBegin + wA - 1;
//
//     // Step size used to iterate through the sub-matrices of A
//     int aStep  = BLOCK_SIZE;
//
//     // Index of the first sub-matrix of B processed by the block
//     int bBegin = BLOCK_SIZE * bx;
//
//     // Step size used to iterate through the sub-matrices of B
//     int bStep  = BLOCK_SIZE * wB;
//
//     // Csub is used to store the element of the block sub-matrix
//     // that is computed by the thread
//     float Csub = 0;
//
//     // Loop over all the sub-matrices of A and B
//     // required to compute the block sub-matrix
//     for (int a = aBegin, b = bBegin;
//          a <= aEnd;
//          a += aStep, b += bStep)
//     {
//
//         // Declaration of the shared memory array As used to
//         // store the sub-matrix of A
//         __shared__ float As[BLOCK_SIZE][BLOCK_SIZE];
//
//         // Declaration of the shared memory array Bs used to
//         // store the sub-matrix of B
//         __shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];
//
//         // Load the matrices from device memory
//         // to shared memory; each thread loads
//         // one element of each matrix
//         As[ty][tx] = A[a + wA * ty + tx];
//         Bs[ty][tx] = B[b + wB * ty + tx];
//
//         // Synchronize to make sure the matrices are loaded
//         __syncthreads();
//
//         // Multiply the two matrices together;
//         // each thread computes one element
//         // of the block sub-matrix
// //#pragma unroll
//
//         for (int k = 0; k < BLOCK_SIZE; ++k)
//         {
//             Csub += As[ty][k] * Bs[k][tx];
//         }
//
//         // Synchronize to make sure that the preceding
//         // computation is done before loading two new
//         // sub-matrices of A and B in the next iteration
//         __syncthreads();
//     }
//
//     // Write the block sub-matrix to device memory;
//     // each thread writes one element
//     int c = wB * BLOCK_SIZE * by + BLOCK_SIZE * bx;
//     C[c + wB * ty + tx] = Csub;
// }



//invrootQeigs <- sqrt(Re(fft(covbase)))
//YfromGamma <- function (Gamma, invrootQeigs, mu){
//    nc <- dim(invrootQeigs)[2]
//    nb <- length(Gamma)
//    return(mu + (1/nb) * Re(fft(invrootQeigs * fft(Gamma, inverse = TRUE))))
//}
