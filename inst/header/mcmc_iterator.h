//#define UPDATE_FREQ 10 // as a percentage

struct iterator {
    int nits;
    int burn;
    int thin;
    int iteration;
    int retain;
};

void initialiseIterator(iterator *iter, int nits, int burn, int thin);
void updateIterator(iterator *iter);
void printIterator(iterator *iter, float h);
int iteration(iterator *iter);
int isRetain(iterator *iter);

void initialiseIterator(iterator *iter, int nits, int burn, int thin){
    iter->nits = nits;
    iter->burn = burn;
    iter->thin = thin;
    iter->iteration = 1;
    iter->retain = 0;
    if(burn==0 && thin == 1){
        iter->retain = 1;
    }
}

void updateIterator(iterator *iter){
    iter->iteration = iter->iteration + 1;
    if(iter->iteration >= iter->burn && (iter->iteration - iter->burn)%iter->thin == 0){
        iter->retain = 1;
    }
}

void printIterator(iterator *iter,float h){
    //int freq = UPDATE_FREQ;

    //if(iter->iteration % (iter->nits / freq) ==0){//(int) floor(100.0f*it/ni) % 1000 == 0){
    if(iter->iteration % 1000 ==0){//(int) floor(100.0f*it/ni) % 1000 == 0){
        printf("Iteration %d of %d, h = %3.3f\n",iter->iteration,iter->nits,h);
    }
    if(iter->iteration == iter->nits){
        printf("\n");
    }
}

int iteration(iterator *iter){
    return iter->iteration;
}

int isRetain(iterator *iter){
    if(iter->iteration <= iter->burn){
        return 0;
    }
    else{
        if((iter->iteration - iter->burn) % iter->thin == 0){
            return 1;
        }
    }

    return 0;
}
