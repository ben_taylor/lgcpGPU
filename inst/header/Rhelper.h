//#include <cufft.h>
//#include <helper_functions.h>
//#include <helper_cuda.h>

////////////////////////////////////////////////////////////////////////
// function declarations
////////////////////////////////////////////////////////////////////////

void intToR(FILE *f, int obj, char oname[]);
void floatToR(FILE *f, float obj, char oname[]);
void vectorToR(FILE *f, float *vec, int length, char oname[]);
void complexVectorToR(FILE *f, cufftComplex *vec, int length, char oname[]);
void realMatrixToR(FILE *f, float *mat, int NX, int NY, char oname[]);
void complexMatrixToR(FILE *f, cufftComplex *vec, int NX, int NY, char oname[]);
void gpu_complexMatrixToR(FILE *f, cufftComplex *d_vec, int NX, int NY, char oname[]);

////////////////////////////////////////////////////////////////////////
// function definitions
////////////////////////////////////////////////////////////////////////

void intToR(FILE *f, int obj, char oname[]){
    fprintf(f,"%s <- %d\n\n",oname,obj);
}


void floatToR(FILE *f, float obj, char oname[]){
    fprintf(f,"%s <- %.9g\n\n",oname,obj);
}


void vectorToR(FILE *f, float *vec, int length, char oname[]){
    int i;

    fprintf(f,"%s <- c(",oname);
    for(i=0;i<length-1;i++){
        fprintf(f, " %.9g ,",vec[i]);
    }
    fprintf(f, " %.9g )\n\n",vec[length-1]);
}



void realMatrixToR(FILE *f, float *mat, int NX, int NY, char oname[]){
    int i;

    fprintf(f,"%s <- matrix( c(",oname);
    for(i=0;i<NX*NY-1;i++){
        fprintf(f, " %.9g ,",mat[i]);
    }
    fprintf(f, " %.9g ), %d, %d)\n\n",mat[NX*NY-1], NX, NY);
}



void complexVectorToR(FILE *f, cufftComplex *vec, int length, char oname[]){
    int i;

    fprintf(f,"%s <- complex(real = c(",oname);
    for(i=0;i<length-1;i++){
        fprintf(f, " %.9g ,",vec[i].x);
    }
    fprintf(f, " %.9g ), imaginary = c(",vec[length-1].x);
    for(i=0;i<length-1;i++){
        fprintf(f, " %.9g ,",vec[i].y);
    }
    fprintf(f, " %.9g ))\n\n",vec[length-1].y);
}



void complexMatrixToR(FILE *f, cufftComplex *vec, int NX, int NY, char oname[]){
    int i;

    fprintf(f,"%s <- matrix( complex(real = c(",oname);
    for(i=0;i<NX*NY-1;i++){
        fprintf(f, " %.9g ,",vec[i].x);
    }
    fprintf(f, " %.9g ), imaginary = c(",vec[NX*NY-1].x);
    for(i=0;i<NX*NY-1;i++){
        fprintf(f, " %.9g ,",vec[i].y);
    }
    fprintf(f, " %.9g )), %d, %d)\n\n",vec[NX*NY-1].y, NX, NY);
}



void gpu_complexMatrixToR(FILE *f, cufftComplex *d_vec, int NX, int NY, char oname[]){
    int i;

    cufftComplex *vec;
    vec = (cufftComplex*)malloc(NX * NY * sizeof(cufftComplex));
    checkCudaErrors( cudaMemcpy(vec,d_vec,NX * NY * sizeof(cufftComplex),cudaMemcpyDeviceToHost) );

    fprintf(f,"%s <- matrix( complex(real = c(",oname);
    for(i=0;i<NX*NY-1;i++){
        fprintf(f, " %.9g ,",vec[i].x);
    }
    fprintf(f, " %.9g ), imaginary = c(",vec[NX*NY-1].x);
    for(i=0;i<NX*NY-1;i++){
        fprintf(f, " %.9g ,",vec[i].y);
    }
    fprintf(f, " %.9g )), %d, %d)\n\n",vec[NX*NY-1].y, NX, NY);

    free(vec);
}
