void sortFloatDescending(float *tosort, int len, float *sorted, int *index);
void sortIntDescending(int *tosort, int len, int *sorted, int *index);
void sampleInt(int *what, float *prob, int numwhat, int *out, int num_to_sample); // "what" is class int

void printArray(float *array, int nrow, int ncol, int depth);
void printArrayR(float *array, int nrow, int ncol, int depth);
void printArrayAsInt(float *array, int nrow, int ncol, int depth);
void printArraySum(float *array, int nrow, int ncol, int depth);
void printMatrix(float *matrix, int nrow, int ncol);
void printMatrixAsInt(float *matrix, int nrow, int ncol);
void printMatrix_cufftComplex(cufftComplex *matrix, int nrow, int ncol);
void printArray_cufftComplex(cufftComplex *matrix, int nrow, int ncol, int depth);
void printArraySum_cufftComplex(cufftComplex *matrix, int nrow, int ncol, int depth);

void checkFloat(float *obj,int nx, int ny, const char *name);
void checkFloatAsInt(float *obj,int nx, int ny, const char *name);
void checkArray(float *obj,int nx, int ny, int nz, const char *name);
void checkArrayR(float *obj,int nx, int ny, int nz, const char *name);
void checkArrayAsInt(float *obj,int nx, int ny, int nz, const char *name);
void checkArraySum(float *obj,int nx, int ny, int nz, const char *name);
void checkInt(int *obj, const char *name);
void checkcufftComplex(cufftComplex *obj,int nx, int ny, const char *name);
void checkcufftComplexArray(cufftComplex *obj,int nx, int ny, int nz, const char *name);
void checkcufftComplexArraySum(cufftComplex *obj,int nx, int ny, int nz, const char *name);


void sortFloatDescending(float *tosort, int len, float *sorted, int *index){ // Insertion sort
    int i, j;

    float temp;
    int tempint;
    for(i=0; i<len; i++){
        sorted[i] = tosort[i]; // copy the vector to be sorted
        index[i] = i;
    }

    for(i=len-2; i>=0; i--){
        j = i;
        while(j<len-1 && sorted[j+1] > sorted[j]){
            temp = sorted[j];
            sorted[j] = sorted[j+1];
            sorted[j+1] = temp;

            tempint = index[j]; // repeat for index
            index[j] = index[j+1];
            index[j+1] = tempint;
            j = j + 1;
        }
    }

}

void sortIntDescending(int *tosort, int len, int *sorted, int *index){ // Insertion sort
    int i, j;

    int temp, tempint;
    for(i=0; i<len; i++){
        sorted[i] = tosort[i]; // copy the vector to be sorted
        index[i] = i;
    }

    for(i=len-2; i>=0; i--){
        j = i;
        while(j<len-1 && sorted[j+1] > sorted[j]){
            temp = sorted[j];
            sorted[j] = sorted[j+1];
            sorted[j+1] = temp;

            tempint = index[j]; // repeat for index
            index[j] = index[j+1];
            index[j+1] = tempint;
            j = j + 1;
        }
    }

}

void sampleInt(int *what, float *prob, int numwhat, int *out, int num_to_sample){
    int i, j;
    float probsum;
    float *wt, *cwt, *probsorted;
    int *index;
    float unif;

    wt = (float*)malloc(numwhat*sizeof(float));
    cwt = (float*)malloc(numwhat*sizeof(float));
    probsorted = (float*)malloc(numwhat*sizeof(float));
    index = (int*)malloc(numwhat*sizeof(int));

    // for(i=0; i<numwhat; i++){
    //     probsorted[i] = prob[i]; // copy prob
    // }

    sortFloatDescending(prob, numwhat, probsorted, index); // sorts probabilities into descending order of magnitude for efficiency

    probsum = 0.0f;
    for(i=0; i<numwhat; i++){
        probsum = probsum + probsorted[i];
    }

    // set up normalised weights and cumulative weights

    for(i=0; i<numwhat; i++){ // weights
        wt[i] = probsorted[i] / probsum;
    }


    cwt[0] = wt[0]; // cumulative weights
    for(i=1; i<numwhat; i++){ // note loop runs from i=1
        cwt[i] = cwt[i-1] + wt[i];
    }

    // printf("\n");
    // for(i=0; i<numwhat; i++){
    //     printf("%3.3f  ",cwt[i]);
    // }
    // printf("\n");

    for(i=0; i<num_to_sample; i++){
        unif = (float) rand() / ((float)RAND_MAX + 1.0f);
        j = 0;
        while(cwt[j] <= unif && j < numwhat - 1){
            j = j + 1;
        }
        out[i] = what[index[j]];
        //printf("%d\n",out[i]);
    }

    free(wt);
    free(cwt);
    free(index);
    free(probsorted);
    wt = NULL;
    cwt = NULL;
    index = NULL;
    probsorted = NULL;
}



void printArray(float *array, int nrow, int ncol, int depth){

    int i, j, k, ind;

    for(k=0; k<depth; k++){
        for (i=0; i<nrow; i++){
            for (j=0; j<ncol; j++){
                ind = i + j*nrow + k*(nrow * ncol); //ijToIndex(i,j,nrow);//i + j*ncol;
                if(j==ncol-1){
                    printf("%3.3f \n",array[ind]);
                    //printf("%d \n",ind);
                }
                else{
                    printf("%3.3f ",array[ind]);
                    //printf("%d ",ind);
                }
            }
        }
        printf("\n");
    }
    printf("\n");
}

void printArrayR(float *array, int nrow, int ncol, int depth){

    int i, j, k, ind;

    FILE *f;
    f = fopen("robj.RData","w");

    fprintf(f,"temp <- list()\n");
    for(k=0; k<depth; k++){
        fprintf(f,"temp[[%d]] <- matrix(c(",k+1);
        for (j=0; j<ncol; j++){
            for (i=0; i<nrow; i++){
                ind = i + j*nrow + k*(nrow * ncol); //ijToIndex(i,j,nrow);//i + j*ncol;
                if(j == ncol -1 && i == nrow -1){
                    fprintf(f,"%3.3f",array[ind]);
                    //printf("%d \n",ind);
                }
                else{
                    fprintf(f,"%3.3f,",array[ind]);
                    //printf("%d ",ind);
                }
            }
        }
        fprintf(f,"),%d,%d)\n",nrow,ncol);
    }
    fprintf(f,"\n");

    fclose(f);
}

void printArrayAsInt(float *array, int nrow, int ncol, int depth){

    int i, j, k, ind;

    for(k=0; k<depth; k++){
        for (i=0; i<nrow; i++){
            for (j=0; j<ncol; j++){
                ind = i + j*nrow + k*(nrow * ncol); //ijToIndex(i,j,nrow);//i + j*ncol;
                if(j==ncol-1){
                    printf("%d \n",(int) array[ind]);
                    //printf("%d \n",ind);
                }
                else{
                    printf("%d ",(int) array[ind]);
                    //printf("%d ",ind);
                }
            }
        }
        printf("\n");
    }
    printf("\n");
}

void printArraySum(float *array, int nrow, int ncol, int depth){

    int i, j, k, ind;
    float sum;

    for(k=0; k<depth; k++){
        sum = 0.0f;
        for (i=0; i<nrow; i++){
            for (j=0; j<ncol; j++){
                ind = i + j*nrow + k*(nrow * ncol); //ijToIndex(i,j,nrow);//i + j*ncol;
                sum = sum + array[ind];
            }
        }
        printf("Sum of %d th array: %.9g\n",k,sum);
    }
    printf("\n");
}

void printMatrix(float *matrix, int nrow, int ncol){

    int i, j, ind;

    for (i=0; i<nrow; i++){
        for (j=0; j<ncol; j++){
            ind = i + j*nrow; //ijToIndex(i,j,nrow);//i + j*ncol;
            if(j==ncol-1){
                printf("%.9g \n",matrix[ind]);
            }
            else{
                printf("%.9g ",matrix[ind]);
            }
        }
    }
    printf("\n");
}

void printMatrixAsInt(float *matrix, int nrow, int ncol){

    int i, j, ind;

    for (i=0; i<nrow; i++){
        for (j=0; j<ncol; j++){
            ind = i + j*nrow; //ijToIndex(i,j,nrow);//i + j*ncol;
            if(j==ncol-1){
                printf("%d \n",(int) matrix[ind]);
            }
            else{
                printf("%d",(int) matrix[ind]);
            }
        }
    }
    printf("\n");
}

void printMatrix_cufftComplex(cufftComplex *matrix, int nrow, int ncol){

    int i, j, ind;

    printf("Real Part:\n\n");
    for (i=0; i<nrow; i++){
        for (j=0; j<ncol; j++){
            ind = i + j*nrow; //ijToIndex(i,j,nrow);//i + j*ncol;
            if(j==ncol-1){
                printf("%3.3f \n",matrix[ind].x);
            }
            else{
                printf("%3.3f ",matrix[ind].x);
            }
        }
    }
    printf("Imaginary Part:\n\n");
    for (i=0; i<nrow; i++){
        for (j=0; j<nrow; j++){
            ind = i + j*nrow; //ijToIndex(i,j,nrow);//i + j*ncol;
            if(j==ncol-1){
                printf("%.9g \n",matrix[ind].y);
            }
            else{
                printf("%.9g ",matrix[ind].y);
            }
        }
    }
    printf("\n");
}

void printArray_cufftComplex(cufftComplex *array, int nrow, int ncol, int depth){

    int i, j, k, ind;

    printf("Real Part:\n\n");
    for(k=0;k<depth;k++){
        for (i=0; i<nrow; i++){
            for (j=0; j<ncol; j++){
                ind = i + j*nrow + k*(nrow * ncol); //ijToIndex(i,j,nrow);//i + j*ncol;
                if(j==ncol-1){
                    printf("%3.3f \n",array[ind].x);
                }
                else{
                    printf("%3.3f ",array[ind].x);
                }
            }
        }
        printf("\n");
    }
    printf("Imaginary Part:\n\n");
    for(k=0;k<depth;k++){
        for (i=0; i<nrow; i++){
            for (j=0; j<nrow; j++){
                ind = i + j*nrow + k*(nrow * ncol); //ijToIndex(i,j,nrow);//i + j*ncol;
                if(j==ncol-1){
                    printf("%.9g \n",array[ind].y);
                }
                else{
                    printf("%.9g ",array[ind].y);
                }
            }
        }
        printf("\n");
    }
    printf("\n");
}


void printArraySum_cufftComplex(cufftComplex *array, int nrow, int ncol, int depth){

    int i, j, k, ind;
    float sum;

    printf("Real Part:\n\n");
    for(k=0;k<depth;k++){
        sum = 0.0f;
        for (i=0; i<nrow; i++){
            for (j=0; j<ncol; j++){
                ind = i + j*nrow + k*(nrow * ncol); //ijToIndex(i,j,nrow);//i + j*ncol;
                sum = sum + array[ind].x;
            }
        }
        printf("Sum of %d th array: %.9g\n",k,sum);
    }
    printf("Imaginary Part:\n\n");
    for(k=0;k<depth;k++){
        sum = 0.0f;
        for (i=0; i<nrow; i++){
            for (j=0; j<nrow; j++){
                ind = i + j*nrow + k*(nrow * ncol); //ijToIndex(i,j,nrow);//i + j*ncol;
                sum = sum + array[ind].y;
            }
        }
        printf("Sum of %d th array: %.9g\n",k,sum);
    }
    printf("\n");
}

void checkFloat(float *obj,int nx, int ny, const char *name){
    float *temp;
    temp = (float*)malloc(nx * ny * sizeof(float));
    //checkCudaErrors( cudaMemcpy(temp,obj,nx * ny * sizeof(float),cudaMemcpyDeviceToHost) );
    cudaMemcpy(temp,obj,nx * ny * sizeof(float),cudaMemcpyDeviceToHost); // no checking ...
    printf("%s:\n",name);
    printMatrix(temp,nx,ny);
    free(temp);
}

void checkFloatAsInt(float *obj,int nx, int ny, const char *name){
    float *temp;
    temp = (float*)malloc(nx * ny * sizeof(float));
    //checkCudaErrors( cudaMemcpy(temp,obj,nx * ny * sizeof(float),cudaMemcpyDeviceToHost) );
    cudaMemcpy(temp,obj,nx * ny * sizeof(float),cudaMemcpyDeviceToHost); // no checking ...
    printf("%s:\n",name);
    printMatrixAsInt(temp,nx,ny);
    free(temp);
}

void checkArray(float *obj,int nx, int ny, int nz, const char *name){
    float *temp;
    temp = (float*)malloc(nx * ny * nz * sizeof(float));
    //checkCudaErrors( cudaMemcpy(temp,obj,nx * ny * sizeof(float),cudaMemcpyDeviceToHost) );
    cudaMemcpy(temp,obj,nx * ny * nz* sizeof(float),cudaMemcpyDeviceToHost); // no checking ...
    printf("%s:\n",name);
    printArray(temp,nx,ny,nz);
    free(temp);
}

void checkArrayR(float *obj,int nx, int ny, int nz, const char *name){
    float *temp;
    temp = (float*)malloc(nx * ny * nz * sizeof(float));
    //checkCudaErrors( cudaMemcpy(temp,obj,nx * ny * sizeof(float),cudaMemcpyDeviceToHost) );
    cudaMemcpy(temp,obj,nx * ny * nz* sizeof(float),cudaMemcpyDeviceToHost); // no checking ...
    printf("%s:\n",name);
    printArrayR(temp,nx,ny,nz);
    free(temp);
}

void checkArrayAsInt(float *obj,int nx, int ny, int nz, const char *name){
    float *temp;
    temp = (float*)malloc(nx * ny * nz * sizeof(float));
    //checkCudaErrors( cudaMemcpy(temp,obj,nx * ny * sizeof(float),cudaMemcpyDeviceToHost) );
    cudaMemcpy(temp,obj,nx * ny * nz* sizeof(float),cudaMemcpyDeviceToHost); // no checking ...
    printf("%s:\n",name);
    printArrayAsInt(temp,nx,ny,nz);
    free(temp);
}

void checkArraySum(float *obj,int nx, int ny, int nz, const char *name){
    float *temp;
    temp = (float*)malloc(nx * ny * nz * sizeof(float));
    //checkCudaErrors( cudaMemcpy(temp,obj,nx * ny * sizeof(float),cudaMemcpyDeviceToHost) );
    cudaMemcpy(temp,obj,nx * ny * nz* sizeof(float),cudaMemcpyDeviceToHost); // no checking ...
    printf("Sum of %s:\n",name);
    printArraySum(temp,nx,ny,nz);
    free(temp);
}



void checkInt(int *obj, int length, const char *name){
    int i, *temp;
    temp = (int*)malloc(sizeof(int));
    //checkCudaErrors( cudaMemcpy(temp,obj,sizeof(int),cudaMemcpyDeviceToHost) );
    cudaMemcpy(temp,obj,sizeof(int),cudaMemcpyDeviceToHost);
    printf("%s:\n",name);
    if(length == 1){
        printf("%d\n",temp[0]);
    }
    else{
        for(i=0;i<(length-1);i++){
            printf("%d  ",temp[length-1]);
        }
        printf("%d\n",temp[length-1]);
    }

    free(temp);
}



void checkcufftComplex(cufftComplex *obj,int nx, int ny, const char *name){
    cufftComplex *temp;
    temp = (cufftComplex*)malloc(nx * ny * sizeof(cufftComplex));
    //checkCudaErrors( cudaMemcpy(temp,obj,nx * ny * sizeof(cufftComplex),cudaMemcpyDeviceToHost) );
    cudaMemcpy(temp,obj,nx * ny * sizeof(cufftComplex),cudaMemcpyDeviceToHost);
    printf("%s:\n",name);
    printMatrix_cufftComplex(temp,nx,ny);
    free(temp);
}

void checkcufftComplexArray(cufftComplex *obj,int nx, int ny, int nz, const char *name){
    cufftComplex *temp;
    temp = (cufftComplex*)malloc(nx * ny * nz * sizeof(cufftComplex));
    //checkCudaErrors( cudaMemcpy(temp,obj,nx * ny * sizeof(cufftComplex),cudaMemcpyDeviceToHost) );
    cudaMemcpy(temp,obj,nx * ny * nz * sizeof(cufftComplex),cudaMemcpyDeviceToHost);
    printf("%s:\n",name);
    printArray_cufftComplex(temp,nx,ny,nz);
    free(temp);
}

void checkcufftComplexArraySum(cufftComplex *obj,int nx, int ny, int nz, const char *name){
    cufftComplex *temp;
    temp = (cufftComplex*)malloc(nx * ny * nz * sizeof(cufftComplex));
    //checkCudaErrors( cudaMemcpy(temp,obj,nx * ny * sizeof(cufftComplex),cudaMemcpyDeviceToHost) );
    cudaMemcpy(temp,obj,nx * ny * nz * sizeof(cufftComplex),cudaMemcpyDeviceToHost);
    printf("%s:\n",name);
    printArraySum_cufftComplex(temp,nx,ny,nz);
    free(temp);
}
