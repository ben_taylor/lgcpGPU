% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/post-process.R
\name{gpuSummary}
\alias{gpuSummary}
\title{gpuSummary function}
\usage{
gpuSummary(out)
}
\arguments{
\item{out}{output of MCMC run}
}
\value{
...
}
\description{
A function to print summary information about a GPU LGCP run
}
